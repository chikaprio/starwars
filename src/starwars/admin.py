from django.contrib import admin

from starwars.models import Collection

admin.site.register(Collection)
