import csv
import uuid

from src.celery import app
from starwars.client import StarWarsAPIClient
from starwars.models import Collection


@app.task
def csv_writer():
    csv_columns = [
        'name', 'height', 'mass', 'hair_color',
        'skin_color', 'eye_color', 'birth_year',
        'gender', 'homeworld', 'date'
    ]
    client = StarWarsAPIClient()
    all_people = client.get_people()
    filename = uuid.uuid4()
    with open(f'media/people/{filename}.csv', 'a+') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=csv_columns, extrasaction='ignore')
        writer.writeheader()
        for person in all_people:
            person['homeworld'] = client.get_homeworld_name(person['homeworld'])
            person['date'] = person['edited'][:10]
            writer.writerow(person)
    collection = Collection.objects.create()
    collection.csv_file.name = f'media/people/{filename}.csv'
    collection.save()
