import math

from starwars.utils import make_request
from django.conf import settings


class StarWarsAPIClient:

    def get_people(self):
        all_people = []
        count = 1
        while True:
            response = make_request(f'{settings.STARWARS_API_BASE_URL}/people/?page={count}')
            total_pages = math.ceil(int(response.json()['count']) / 10)
            for person in response.json()['results']:
                all_people.append(person)
            count += 1
            if count > total_pages:
                break
        return all_people

    def get_homeworld_name(self, planet_url):
        return make_request(planet_url).json()['name']
