from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import views, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import serializers as rest_serializers

from starwars.serializers import CollectionSerializer
from starwars.tasks import csv_writer
from starwars.models import Collection
from starwars.utils import _csv_sorter


class FetchCollectionAPIView(views.APIView):

    def get(self, request, format=None):
        csv_writer.delay()
        return Response({'detail': 'Success'})


class CollectionsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer

    @swagger_auto_schema(
        operation_summary='Count combinations of occurences by specific fields',
        operation_description='Returns counter of occurences if they are specified, else returns all',
        manual_parameters=[
            openapi.Parameter(
                'count',
                openapi.IN_QUERY,
                description="Group by. Available params "
                            "[name, birth_year, homeworld, height, mass, "
                            "hair_color, skin_color, eye_color, gender, date]",
                type=openapi.TYPE_STRING,
                required=False
            ),
            openapi.Parameter(
                'limit',
                openapi.IN_QUERY,
                description="Limit of content",
                type=openapi.TYPE_STRING,
                required=False
            )
        ],
    )
    @action(detail=True, methods=['get'])
    def read_data(self, request, pk=None):
        limit = request.query_params.get('limit')
        try:
            limit = int(limit) if limit else 10
        except ValueError:
            raise rest_serializers.ValidationError(
                {'limit': 'Invalid query parameter'}
            )
        csv_data = self.get_object().get_csv_data()
        data = _csv_sorter(csv_data, request.query_params, limit)
        return Response(data=data)
