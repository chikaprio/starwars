import requests
from itertools import groupby

from starwars.exceptions import ResourceDoesNotExist


def make_request(url):
    response = requests.get(url)
    if response.status_code != 200:
        raise ResourceDoesNotExist('Resource does not exist')
    return response


def _csv_sorter(csv_data, query_params, limit):
    keys = query_params.getlist('count')
    if keys:
        list_sorter = lambda x: tuple(x[k] for k in keys)
        grouper = lambda x: tuple(x[k] for k in keys)
        results = [
            {**dict(zip(keys, k)), 'count': len([*g])}
            for k, g in
            groupby(sorted(csv_data, key=list_sorter), grouper)
        ]
        return {'results': results}
    return {
        'count': len(csv_data),
        'limit': limit,
        'results': csv_data[:limit]
    }

