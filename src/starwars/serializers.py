from rest_framework import serializers

from starwars.models import Collection


class CollectionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Collection
        fields = ('id', 'url', 'csv_file', 'timestamp')
