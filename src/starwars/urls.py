from django.urls import path, include
from rest_framework.routers import DefaultRouter

from starwars import views


router = DefaultRouter()
router.register('collections', views.CollectionsViewSet, basename='collection')

v1_patterns = [
    path('fetch-collection/', views.FetchCollectionAPIView.as_view(), name='fetch_collection'),
]

v1_patterns += router.urls


urlpatterns = [
    path('v1/', include(v1_patterns)),
]
