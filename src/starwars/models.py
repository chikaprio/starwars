import csv
import json
from django.db import models


class Collection(models.Model):
    csv_file = models.FileField(upload_to='people', null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return str(self.timestamp)

    def get_csv_data(self):
        with self.csv_file.open(mode='r') as csv_file:
            reader = csv.DictReader(csv_file)
            return json.loads(json.dumps(list(reader)))
